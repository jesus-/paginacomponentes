from django.contrib import admin
from administracion.models import Trainer, User, Suscripcion, Servicio

# Register your models here.
admin.site.register(Trainer)
admin.site.register(User)
admin.site.register(Suscripcion)
admin.site.register(Servicio)