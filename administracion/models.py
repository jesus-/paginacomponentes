from django.db import models
from django.urls import reverse
from django.db.models.signals import post_delete
from django.dispatch import receiver

# Create your models here.
class Trainer(models.Model):
    """ Entrenador  """
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='trainers/')
    pub_date = models.DateField(auto_now_add=True)
    height = models.DecimalField(max_digits=3, decimal_places=2)
    weight = models.IntegerField()
    number_users = models.IntegerField()
    comment = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.first_name + " " + self.last_name
    
    def get_absolute_url(self):
        return reverse('trainer-list')    
    

class User(models.Model):
    """ Usuarios """
    trainer = models.ForeignKey('Trainer', on_delete=models.PROTECT,related_name='get_users' )
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='users/')
    pub_date = models.DateField(auto_now_add=True)
    height = models.DecimalField(max_digits=3, decimal_places=2)
    weight = models.IntegerField()
    comment = models.CharField(max_length=200, blank=True)
    
    def __str__(self):
        return self.first_name + " " + self.last_name
    
    def get_absolute_url(self):
        return reverse('user-list')
    
    def get_tiquetera(self):
        return self.tiquetera_set.first()
    
class Suscripcion(models.Model):
    user = models.ForeignKey('User', primary_key=True, on_delete=models.PROTECT, related_name='get_users')
    servicio = models.ForeignKey('Servicio', on_delete=models.PROTECT,related_name='get_suscripciones' )
    comment = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.servicio.name
    
    def get_absolute_url(self):
        return reverse('suscripcion-list')


class Servicio(models.Model):
    """ Servicios  """
    name = models.CharField(max_length=50)
    valor = models.IntegerField()
    comment = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('servicio-list')

    
