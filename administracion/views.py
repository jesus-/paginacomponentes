from django.shortcuts import render
from django.urls import reverse_lazy 
from django.views.generic import CreateView, DeleteView, UpdateView, ListView, DetailView
from administracion.models import Suscripcion, Trainer, User, Servicio
from rest_framework import viewsets
from .serializers import ServicioSerializer, TrainerSerializer, UserSerializer, SuscripcionSerializer
# Create your views here.

class ServicioListView(ListView):
    model = Servicio

class SuscripcionListView(ListView):
    model = Suscripcion

class SuscripcionListView(ListView):
    model = Suscripcion
 
class SuscripcionDetailView(DetailView):
    model = Suscripcion

class TrainerListView(ListView):
    model = Trainer

class TrainerDetailView(DetailView):
    model = Trainer


class UserListView(ListView):
    model = User

class UserDetailView(DetailView):
    model = User
    def get_context_data(self, **kwargs):
        pk = self.kwargs['pk']
        # Obtener la suscripcion del usuario
        suscripcion_usuario = Suscripcion.objects.filter(user=pk).first()

        # Agregar la suscripcion del usuario al contexto
        context = super().get_context_data(**kwargs)
        context['suscripcion_usuario'] = suscripcion_usuario

        return context
    
class UserUpdate(UpdateView):
    model = User
    fields = '__all__' 

class UserCreate(CreateView):
    model = User
    fields = '__all__'

class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('user-list')    

class SuscripcionCreate(CreateView):
    model = Suscripcion
    fields = '__all__'

class SuscripcionDelete(DeleteView):
    model = Suscripcion
    success_url = reverse_lazy('suscripcion-list')      

class ServicioCreate(CreateView):
    model = Servicio
    fields = '__all__'

class ServicioDelete(DeleteView):
    model = Servicio
    success_url = reverse_lazy('servicio-list')        

class ServicioUpdate(UpdateView):
    model = Servicio
    fields = '__all__' 

class TrainerCreate(CreateView):
    model = Trainer
    fields = '__all__'

class TrainerDelete(DeleteView):
    model = Trainer
    success_url = reverse_lazy('trainer-list')        

class TrainerUpdate(UpdateView):
    model = Trainer
    fields = '__all__' 



#Buscar
def search(request):
    # Obtener la consulta de búsqueda
    query = request.GET.get("q")

    # Recuperar los datos de los entrenadores o usuarios que coincidan con la consulta de búsqueda
    trainers = Trainer.objects.filter(first_name__icontains=query)
    users = User.objects.filter(first_name__icontains=query)

    # Devolver los datos
    return render(request, "search.html", {
        "trainers": trainers,
        "users": users,
    }) 

class SuscripcionViewSet(viewsets.ModelViewSet):
    queryset = Suscripcion.objects.all()
    serializer_class = SuscripcionSerializer

class TrainerViewSet(viewsets.ModelViewSet):
    queryset = Trainer.objects.all()    
    serializer_class = TrainerSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()    
    serializer_class = UserSerializer

class ServicioViewSet(viewsets.ModelViewSet):
    queryset = Servicio.objects.all()    
    serializer_class = ServicioSerializer


